list_of_dirs = []


class Dir:
    items = []
    parent = None
    child_dirs = {}

    def __init__(self, name):
        self.name = name
        self.child_dirs = {}
        self.items = []
        list_of_dirs.append(self)

    def add_item(self, a):
        self.items.append(a)

    def add_child_dir(self, child_dir):
        self.child_dirs[child_dir.name] = child_dir
        child_dir.parent = self

    def get_child_dir(self, name):
        return self.child_dirs.get(name)

    def get_parent_dir(self):
        return self.parent

    def get_size(self):
        total_size = 0
        for child_dir_name in self.child_dirs:
            total_size += self.child_dirs[child_dir_name].get_size()
        for item in self.items:
            total_size += item[0]
        return total_size


with open("input/input7.txt", "r") as f:
    inp = [x.strip() for x in f.readlines()]
    current_directory = Dir("")
    outermost_directory = Dir("/")
    current_directory.add_child_dir(outermost_directory)
    for command in inp:
        command = command.split()
        if command[0] == "$" and command[1] == "cd":
            if command[2] == "/":
                current_directory = outermost_directory
            elif command[2] == "..":
                current_directory = current_directory.get_parent_dir()
            else:
                current_directory = current_directory.get_child_dir(command[2])
        elif command[0] == "dir":
            current_directory.add_child_dir(Dir(command[1]))
        elif command[0] != "$":
            command = [int(command[0]), command[1]]
            current_directory.add_item(command)
    ex1 = sum([d.get_size() for d in list_of_dirs if d.get_size() < 100000])
    space_to_free_up = 30000000 - (70000000 - outermost_directory.get_size())
    removable_dirs = sorted([d.get_size() for d in list_of_dirs if d.get_size() > space_to_free_up])
    ex2 = removable_dirs[0]
    print(f"Exercise 1: {ex1}")
    print(f"Exercise 2: {ex2}")
