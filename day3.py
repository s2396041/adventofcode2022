with open("input/input3.txt", "r") as f:
    inp = [x.strip() for x in f.readlines()]
    def char_score(x): return ord(x) - 96 if x.islower() else (ord(x) - 64 + 26)
    ex1, ex2 = 0, 0
    for l in inp:
        first_halve, second_halve = l[:len(l)//2], l[len(l)//2:]
        matching_items = set([a for a in first_halve if a in second_halve])
        ex1 += sum([char_score(x) for x in matching_items])
    groups = [[set(y) for y in inp[i:i + 3]] for i in range(0, len(inp), 3)]
    for group in groups:
        shared_letters = group[0]
        shared_letters.intersection_update(group[1])
        shared_letters.intersection_update(group[2])
        ex2 += sum([char_score(a) for a in shared_letters])
    print(f"Exercise 1: {ex1}")
    print(f"Exercise 2: {ex2}")
