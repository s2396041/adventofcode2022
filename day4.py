with open("input/input4.txt", "r") as f:
    f = [[[int(z) for z in y.split("-")] for y in x.strip().split(',')] for x in f.readlines()]
    ex1, ex2 = 0, 0
    for pair in f:
        x, y = pair
        overlap = list(range(max(x[0], y[0]), min(x[-1], y[-1])+1))
        ex1 += len(overlap) >= len(list(range(x[0], x[1]+1))) or len(overlap) >= len(list(range(y[0], y[1]+1)))
        ex2 += len(overlap)>0
    print(f"Exercise 1: {ex1}")
    print(f"Exercise 2: {ex2}")