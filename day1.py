with open("input/input1.txt", "r") as f:
    inp = sorted([sum([int(number) for number in group.split()]) for group in "".join(f.readlines()).split("\n\n")])
    ex1 = inp[-1]
    ex2 = sum(inp[-3:])
    print(f"Exercise 1: {ex1}")
    print(f"Exercise 2: {ex2}")
