def get_score(o_move, y_move):
    r = score_for_shape[y_move]
    if y_move == draw_shape[o_move]:
        r += 3
    elif y_move == win_shape[o_move]:
        r += 6
    return r


with open("input/input2.txt", "r") as f:
    inp = [x.strip().split() for x in f.readlines()]
    score_for_shape = {"X": 1, "Y": 2, "Z": 3}
    draw_shape = {"A": "X", "B": "Y", "C": "Z"}
    win_shape = {"A": "Y", "B": "Z", "C": "X"}
    lose_shape = {"A": "Z", "B": "X", "C": "Y"}
    ex1, ex2 = 0, 0
    for game in inp:
        ex1 += get_score(game[0], game[1])
        ex2 += get_score(game[0], {"X": lose_shape[game[0]], "Y": draw_shape[game[0]], "Z": win_shape[game[0]]}[game[1]])
    print(f"Exercise 1: {ex1}; Exercise 2: {ex2}")
