def find_marker(marker_size):
    with open("input/input6.txt", "r") as f:
        inp = [x.strip() for x in f.readlines()][0]
        for c in range(len(inp) - (marker_size-1)):
            marker = set(inp[c:c + marker_size])
            if len(marker) == marker_size:
                return c + marker_size
    return -1


ex1 = find_marker(4)
ex2 = find_marker(14)
print(f"Exercise 1: {ex1}")
print(f"Exercise 2: {ex2}")
