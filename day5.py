class Pile:
    def __init__(self, data):
        self.items = "".join(reversed(data)).strip()

    def move(self, amount, target, r):
        if r:
            target.items += self.items[-amount:][::-1]
        else:
            target.items += self.items[-amount:]
        self.items = self.items[:-amount]


def execute_instructions(r=True):
    with open("input/input5.txt", "r") as f:
        inp = [x for x in "".join(f.readlines()).split("\n\n")]
        current_situation, instructions = inp
        current_situation = [list(x) for x in current_situation.split("\n")]
        current_situation = list(zip(*current_situation))
        current_situation = {int(x[-1]): Pile(x[:-1]) for x in current_situation if x[-1].isdigit()}
        for instruction in instructions.split("\n")[:-1]:
            ins = instruction.split()
            amount, start_pos, end_pos = int(ins[1]), int(ins[3]), int(ins[5])
            current_situation[start_pos].move(amount, current_situation[end_pos], r)
        return "".join([v.items[-1] for v in current_situation.values()])


ex1 = execute_instructions(r=True)
ex2 = execute_instructions(r=False)
print(f"Exercise 1: {ex1}")
print(f"Exercise 2: {ex2}")
